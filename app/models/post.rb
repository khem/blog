class Post < ActiveRecord::Base
 #associations
 has_many :comments, dependent: :destroy
 
 #validation rules
 validates_presence_of :title
 validates_presence_of :body
 
end
