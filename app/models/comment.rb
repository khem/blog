class Comment < ActiveRecord::Base
 #associations
 belongs_to :post
 
 #validations
 validates_presence_of :post_id
 validates_presence_of :body
end
